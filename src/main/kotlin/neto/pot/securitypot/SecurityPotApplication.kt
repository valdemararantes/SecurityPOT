package neto.pot.securitypot

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class SecurityPotApplication

fun main(args: Array<String>) {
    SpringApplication.run(SecurityPotApplication::class.java, *args)
}
