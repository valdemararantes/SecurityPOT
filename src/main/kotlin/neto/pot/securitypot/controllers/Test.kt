package neto.pot.securitypot.controllers

import neto.pot.securitypot.sec.Executor
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalTime

@RestController
@RequestMapping(value = ["test", "rest"])
class Test {

    private val log = LoggerFactory.getLogger(javaClass)

    @GetMapping("hello")
    fun hello() = "Olá! [${LocalTime.now()}]".also {
        log.info("Executed")
        log.info("user: ${Executor.name}")
        log.info("roles: ${Executor.roles}")
    }
}