package neto.pot.securitypot.sec

import org.springframework.security.core.context.SecurityContextHolder

object Executor {
    val name: String
        get() = try {
            val auth = SecurityContextHolder.getContext().authentication
            auth.name
        } catch (e: Exception) {
            "Undefined_Executor"
        }
    val roles: String
        get() = try {
            SecurityContextHolder.getContext().authentication.authorities.joinToString()
        } catch (e: Exception) {
            "Undefined roles"
        }
}
