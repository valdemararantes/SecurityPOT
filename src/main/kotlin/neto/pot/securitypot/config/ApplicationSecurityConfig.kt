package neto.pot.securitypot.config

import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.security.SecurityProperties
import org.springframework.context.annotation.Configuration
import org.springframework.core.annotation.Order
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

/**
 * Created by CertibioDemonstracao on 16/12/2016.
 */
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
@Configuration
class ApplicationSecurityConfig : WebSecurityConfigurerAdapter() {

    private val log = LoggerFactory.getLogger(javaClass)

    override fun configure(http: HttpSecurity) {
        log.info("Configuring HttpSecurity ****************************")
        http.csrf().disable().authorizeRequests()
                .antMatchers("/test/**").denyAll()
                .antMatchers("/test/**").hasRole("USER")
                //.antMatchers("/rest/user/save").permitAll()

                //.antMatchers("/test/**").permitAll()

                .antMatchers(HttpMethod.POST, "/login").permitAll()
                .and().cors()
                .and()
                .httpBasic()
    }
}
